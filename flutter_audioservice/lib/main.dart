
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:assets_audio_player/assets_audio_player.dart';
import 'package:flutter_audioservice/DataModel.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  var list = List<DataModel>();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    for(int i = 0; i < 10; i++){
      list.add(DataModel("https://www.kozco.com/tech/LRMonoPhase4.mp3", false, 0.0, '0:0:0', '0:0:0','',''));
    }
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(""),
      ),
      body: Container(
        width: double.infinity,
        height: double.infinity,
        child: ListView.builder(
            shrinkWrap: true,
            // physics: NeverScrollableScrollPhysics(),
            itemCount: 10,
            itemBuilder: (context,position){
              return Padding(padding: EdgeInsets.only(left: 8,right: 8,top: 8),
              child: Container(
                width: double.infinity,
                height: 75,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Row(
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        Expanded(child: AudioWidget.network(
                          url: "https://www.kozco.com/tech/LRMonoPhase4.mp3",
                          play: list[position].play,
                          child: GestureDetector(
                            child:  Container(
                              width: 40,
                              height: 40,
                              child: Icon(list[position].play? Icons.pause :Icons.play_arrow, size: 23,color: Colors.white,),
                              decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  color: Color(0xff3670E5)),
                            ),
                            onTap: (){
                              setState(() {
                                list[position].play = !list[position].play;
                              });
                            },
                          ),
                          onReadyToPlay: (duration) {
                            //onReadyToPlay
                            print('Duration '+duration.inSeconds.toString());
                          },
                          onPositionChanged: (current, duration) {
                            //onPositionChanged
                            print("Duration3 "+current.inSeconds.toString());
                            print('Duration2 '+duration.inSeconds.toString());
                            setState(() {
                              list[position].current = current;
                              list[position].duration = duration;
                              list[position].total_sec = list[position].duration.inHours.toString()+":"+list[position].duration.inMinutes.toString()+":"+list[position].duration.inSeconds.toString();
                              list[position].current_sec = list[position].current.inHours.toString()+":"+list[position].current.inMinutes.toString()+":"+list[position].current.inSeconds.toString();
                              list[position].progressValue = ((list[position].current.inSeconds / list[position].duration.inSeconds) * 100).roundToDouble();
                              print( list[position].progressValue .toString());
                            });
                          },
                          onFinished: (){
                            setState(() {
                              list[position].play = false;
                            });
                          },
                        ),flex: 0,),
                        Expanded(child: LinearPercentIndicator(
                          lineHeight: 2.0,
                          percent: double.parse(list[position].progressValue.toString()) / 100,
                          backgroundColor: Colors.grey,
                          progressColor: Colors.blue,
                        ),flex: 5,)

                      ],
                    ),
                    Align(
                      alignment:Alignment.topRight,
                      child:  Padding(
                        padding: EdgeInsets.only(right: 8),
                        child: Text(list[position].current_sec+"/"+list[position].total_sec),
                      ),
                    )
                  ],
                ),
                decoration: BoxDecoration(
                    color: Colors.white70,
                    borderRadius: BorderRadius.all(Radius.circular(8))
                ),),);
            })
      ),

    );
  }
}
/*Row(
          children: [
            FlatButton(onPressed: () async {
              print("state "+ assetsAudioPlayer.isPlaying.toString());
               if(state == AudioState.START){
                 try {
                   await assetsAudioPlayer.open(
                     Audio.network( "https://www.kozco.com/tech/LRMonoPhase4.mp3"),
                   );
                 } catch (t) {
                   //mp3 unreachable
                 }
                 setState(() {
                   state = AudioState.PAUSE;
                 });
               }else if(state == AudioState.PAUSE){
                 assetsAudioPlayer.pause();
                 setState(() {
                   state = AudioState.PLAY;
                 });
               }else{
                 assetsAudioPlayer.play();
                 setState(() {
                   state = AudioState.PAUSE;
                 });
               }
            }, child: Text(state==AudioState.START || state == AudioState.PAUSE ? 'PLAY' : 'Pause'))
          ],
        )*/